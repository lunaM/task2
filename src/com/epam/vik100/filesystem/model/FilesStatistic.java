package com.epam.vik100.filesystem.model;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

public class FilesStatistic {
	private HashMultiset<String> filesAndFolders;
	private int count = 0;
	private String path = "D:\\vik\\js";

	public FilesStatistic(String path) {
		this.path = path;
	}

	public void collectStatistic() {
		filesAndFolders = HashMultiset.create();
		Path start = Paths.get(path);
		try (Stream<Path> stream = Files.walk(start)) {
			stream.forEach(pathelement -> {
				filesAndFolders.add(pathelement.getFileName().toString());
				count++;
			});
		} catch (Exception e) {
			e.printStackTrace();
		};
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("\n* * * PATH - ");
		result.append(path);
		result.append(" * * *\n");
		result.append("Total number - ");
		result.append(count);
		result.append("\nDistinct - ");
		result.append(filesAndFolders.entrySet().size());	
		return result.toString();
	}
	public void printStatistic(){
		for(Multiset.Entry<String> elem: filesAndFolders.entrySet()){
			System.out.println(elem.getElement()+" "+elem.getCount());
		}
	}
}
